// dependencies
const express = require('express')
const stylus = require('stylus')
const pug = require('pug')
const path = require('path')

// initialize express
const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// stylus middleware
app.use(stylus.middleware(path.join(__dirname, 'public')));

// static files
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.use('/', require('./routes/index'));
app.use('/vitamin-puzzle', require('./routes/puzzle'));
app.use('/dist', express.static(__dirname + '/node_modules/paper/dist'))

// default port
const port = 8000;

// start listening
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
