# Instructions:
* open up a terminal within the of that project (next steps assume that you have already installed Node.js on your system)
* run `npm install`, in order to install the required modules
* run `npm start` or `npm run start`, in order to start the server
* visit http://localhost:8000/ in your web-browser
