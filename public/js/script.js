'use strict'

// get hash string
let hash = window.location.hash.substring(1);

// some common vitamin properties
const vitaminConfig = {
  // colour groups
  groups: {
    B: '#232323', // black
    G: '#a1a1a4', // gray
    W: '#f8fbdb' // white
  },
  radius: 96, // the radius of every single vitamin
  padding: 20, // padding between vitamins
  minPoints: 3 // minimum number of vertices
}

// constructor object
class Vitamin {
  constructor(points, group) {
    this.points = points;
    this.group = group;
    this.mini = false
    this.maxi = false
    this.swaps = []
    this.paper = {}
  }
}

// global vitamin methos
const Vitamins = {
  all: [],
  history: [],

  init() {
    // delete old vitamins
    this.reset()

    // create new vitamins based on our hash string
    for (let i = 0; i < hash.length; i++) {
      const points = this.config.minPoints + i;
      const group = hash.charAt(i);

      this.add(points, group)
    }

    this.setMiniMaxi() // set mini/maxi vitamin properties
    this.setSwapGroups() // set swap groups for maxi-vitamins
  },

  draw() {
    const self = this;
    // get the canvas element
    const canvas = document.getElementById('puzzle');
    // create an empty project and a view for the canvas
    paper.setup(canvas);

    Vitamins.all.forEach(vitamin => {
      const points = vitamin.points;
      const padding = Vitamins.config.padding;

      vitamin.paper = new paper.Path.RegularPolygon(new paper.Point(0, 0), vitamin.points, Vitamins.config.radius)

      vitamin.paper.fillColor = Vitamins.config.groups[vitamin.group]
      vitamin.paper.strokeColor = '#666'
      vitamin.paper.strokeWidth = 1

      // rotate the hexagon, so it looks better, just like in the video
      if (points == 6) vitamin.paper.rotate(30)

      if (points < 4) {
        vitamin.paper.bounds.left = padding
      } else if (points > 3) {
        vitamin.paper.bounds.left = Vitamins.all[points - 4].paper.bounds.right + padding
      }

      // align all vitamins to bottom
      vitamin.paper.bounds.bottom = 192

      // buttons
      const buttonR = 16;

      const buttonX = vitamin.paper.bounds.centerX;
      const buttonY = vitamin.paper.bounds.bottomCenter.y - buttonR * 2;
      const swaps = vitamin.swaps;

      if (swaps.length === 1) {
        self.drawButton(buttonX, buttonY, buttonR, swaps[0], vitamin.points)
      } else if (swaps.length === 2) {
        self.drawButton(buttonX - buttonR * 1.5, buttonY, buttonR, swaps[0], vitamin.points)
        self.drawButton(buttonX + buttonR * 1.5, buttonY, buttonR, swaps[1], vitamin.points)
      }
    })

    // draw the view
    paper.view.draw();

    // leaving this one here temporarily
    // in order to visualize vitamin line objects to other developer
    console.log(this.all)
  },

  drawButton(x, y, r, c, v) {
    const button = new paper.Path.Circle(new paper.Point(x, y), r);

    button.fillColor = Vitamins.config.groups[c]
    button.strokeColor = '#444'
    button.strokeWidth = 1

    // set its fill color to red:
    button.onClick = event => {
      hash = hash.replaceAt(v - 3, c)
      window.location.hash = hash
    }
  },

  // add new vitamin
  add(points, group) {
    return this.all.push(new Vitamin(points, group))
  },

  // get vitamin by points
  getByPoints(points) {
    return this.all.filter(vitamin => vitamin.points === points)[0]
  },

  // get vitamins by group
  getByGroup(group) {
    return this.all.filter(vitamin => vitamin.group === group)
  },

  // get non-empty groups by mapping vitamin groups and returning only unique values
  getNonEmptyGroups(empty) {
    const groups = this.all.map(obj => obj.group);
    return groups.filter((key, val) => groups.indexOf(key) === val)
  },

  // return array of maxi-vitamins, which are smaller than the vitamin, passed as an argument
  getMaxiVitaminsSmallerThan(vitamin) {
    return this.maxies.filter(obj => obj.points < vitamin.points)
  },

  // get colour groups without any vitamins
  getEmptyGroups() {
    const groups = this.all.map(vitamin => vitamin.group);
    return Object.keys(this.config.groups).filter(g => !groups.includes(g));
  },

  // set mini/maxi vitamin properties
  setMiniMaxi() {
    let groups = this.getNonEmptyGroups()

    for (let group in groups) {
      group = this.getByGroup(groups[group])

      const groupMini = this.getByPoints(this.findMiniVitaminsInGroup(group));
      const groupMaxi = this.getByPoints(this.findMaxiVitaminsInGroup(group));

      groupMini.mini = true
      groupMaxi.maxi = true
    }
  },

  // set swap groups for maxi-vitamins
  setSwapGroups() {
    const self = this;
    // reset the swap property of all mini vitamins
    this.minies.forEach(vitamin => {
      vitamin.swaps = []
    })

    this.maxies.forEach(vitamin => {
      const smaller = self.getMaxiVitaminsSmallerThan(vitamin);
      smaller.forEach(smallerVitamin => {
        vitamin.swaps.push(smallerVitamin.group)
      })
      vitamin.swaps = vitamin.swaps.concat(self.getEmptyGroups())
    })
  },

  // find mini vitamins in a group
  findMiniVitaminsInGroup(group) {
    return group.reduce((min, p) => p.points < min ? p.points : min, group[0].points)
  },

  // find maxi vitamins in a group
  findMaxiVitaminsInGroup(group) {
    return group.reduce((max, p) => p.points > max ? p.points : max, group[0].points)
  },

  // sort vitamins by power
  sortVitaminsByPower(vitamins) {
    return vitamins.concat().sort((a, b) => Number(b.points) - Number(a.points));
  },

  // delete all vitamins data and remove the canvas drawing
  reset(callback) {
    return new Promise((resolve, reject) => {
      paper.remove()
      this.all = []
    })
  },

  // some getters
  get minies() {
    return this.all.filter(obj => obj.mini === true)
  },
  get maxies() {
    return this.all.filter(obj => obj.maxi === true)
  },
  get config() {
    return vitaminConfig
  },
};

function testUri(hash) {
  // regular expression matching valid vitamin colour groups
  const hashPattern = new RegExp('[^BWG]');

  // simple client-side error handling of invalid hash patterns
  if (hashPattern.test(hash)) {
    console.error('Bad vitamin pattern.')
    alert('Something went wrong. Probably a bad vitamin pattern. Please check your console for errors.')
    throw new Error("The hash string must contain only valid vitamin colours. Valid colours are ['B', 'W', 'G'].")
  }
}

// update hash history
function updateHashHistory(){
  Vitamins.history.push(hash)
  document.querySelector('#history').innerHTML += hash + '<br>'
}

// string prototype that replaces a character at specific index
String.prototype.replaceAt = function(i, str) {
  return this.substr(0, i) + str + this.substr(i + str.length);
}


// seems like someone didn't know about this one
window.onhashchange = () => {
  hash = window.location.hash.substring(1)
  testUri(hash)
  updateHashHistory()
  Vitamins.init()
  Vitamins.draw()
}

// let's start drawing
window.onload = () => {
  testUri(hash)
  Vitamins.init()
  updateHashHistory()
  Vitamins.draw()
}
